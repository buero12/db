<?

  /**
   * In SQL gibt es verschiedene Bedingungen die jeweils per AND oder OR zusammen gesetzt werden können
   * Mit Klammern können Bedingungen auch zusammen gefasst werden
   * 
   * @see http://dev.mysql.com/doc/refman/5.7/en/comparison-operators.html
   */

  // Einfache Bedingunge
  $sql = '... WHERE id = 1';
  $sql = '... WHERE id != 1';
  $sql = '... WHERE id <> 1';
  
  // Größer/Kleiner als
  $sql = '... WHERE id > 100';
  $sql = '... WHERE id <= 200';
  
  // Zwischen zwei Werten
  $sql = '... WHERE id BETWEEN 100 AND 200';
  $sql = '... WHERE id NOT BETWEEN 100 AND 200';
  
  // Auf eine Liste möglicher Werte prüfen
  $sql = '... WHERE id IN(1, 5, 9)';
  
  // Auf leere Werte prüfen
  $sql = '... WHERE id IS NULL';
  $sql = '... WHERE id IS NOT NULL';
  
  // Werte suchen
  $sql = '... WHERE country LIKE "%land%"';
  $sql = '... WHERE country NOT LIKE "%land%"';
  
  $sql = '... WHERE first_name LIKE "Lu_as"';