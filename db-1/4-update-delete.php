<?

  // Verbindung
  $pdo = include '../connection.php';
  
  /**
   * Updates können mittels Bedingungen beschränkt werden
   */
  $stmt = $pdo->prepare('UPDATE cds SET jahr = :j WHERE interpret = :i');
  
  $stmt->execute([
    ':i' => 'Helene Fischer',
    ':j' => 2010
  ]);
  
  /**
   * Selbes gilt für das Löschen von Datensätzen
   */
  $stmt = $pdo->prepare('DELETE FROM cds WHERE interpret = :i');
  
  $stmt->execute([
    ':i' => 'Helene Fischer'
  ]);