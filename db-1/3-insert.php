<?

  // Verbindung
  $pdo = include '../connection.php';
  
  /**
   * Datensätze lassen sich auch über Prepared Statments einfügen
   */
  
  $stmt = $pdo->prepare('INSERT INTO cds (titel, interpret, jahr) VALUES (:t, :i, :j)');
  $stmt = $pdo->prepare('INSERT INTO cds SET title = :t, interpret = :i, jahr = :j');
  
  $stmt->execute([
    ':t' => 'Von hier bis unendlich',
    ':i' => 'Helene Fischer',
    ':j' => 2006
  ]);
  
  $stmt->execute([
    ':t' => 'So nah wie du',
    ':i' => 'Helene Fischer',
    ':j' => 2007
  ]);