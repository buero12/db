<?

  // Verbindung
  $pdo = include '../connection.php';

  /**
   * Prepared Statments oder vorbereitete Anweisungen
   * Keine Parameterwerte, da diese erst später an Platzhalter gebunden werden
   * SQL-Injections werden verhindert, da Parameter validiert werden
   * Geschwindigkeitsvorteile, wenn die selbe Anweisung mit unterschiedlichen Werten ausgeführt wird
   */
  
  $stmt = $pdo->prepare('SELECT * FROM cds');
  $stmt->execute();
  
  $results = $stmt->fetchAll();
    
  print '<pre>';
  print_r($results);
  print '</pre>';
  
  /**
   * Platzhalter und Parameter
   */
  
  $stmt = $pdo->prepare('SELECT * FROM cds WHERE jahr > :j');
  $stmt->execute([
    ':j' => 2000
  ]);
  
  $results = $stmt->fetchAll();
  
  print '<pre>';
  print_r($results);
  print '</pre>';
  
  $stmt = $pdo->prepare('SELECT * FROM cds WHERE jahr > :j1 AND jahr < :j2');
  $stmt->execute([
    ':j1' => 2000,
    ':j2' => 2010
  ]);
  
  $results = $stmt->fetchAll();
  
  print '<pre>';
  print_r($results);
  print '</pre>';
  
  /**
   * Sortierung und Limits
   */
  
  $stmt = $pdo->prepare('SELECT * FROM cds WHERE jahr > :j ORDER BY jahr LIMIT 0, 2');
  $stmt->execute([
    ':j' => 2000
  ]);
  
  $results = $stmt->fetchAll();
  
  print '<pre>';
  print_r($results);
  print '</pre>';