<?

  /**
   * SQL bietet eine Vielzahl von unterschiedlichen Datentypen, welche immer ausgenutzt werden sollten
   * 
   * @see http://dev.mysql.com/doc/refman/5.7/en/data-types.html
   */

  // Varchar: Texte bis 255 Zeichen
  // Man sollte die maximal Anzahl der Zeichen nicht immer voll ausreizen
  // Namen bspw. sind in der Regel nie 255 Zeichen lang, daher können diese einfach als 32 bis 64 Zeichen reserviert werden
  // Je mehr Zeichen reserviert werden umso mehr Speicher verbraucht die Datenbank

  // Text: Texte bis 64.000 Zeichen
  // Textfelder lassen sich nicht als Schlüssel verwenden und sollten daher auch keine solche Daten enthalten

  // Int: Ganzzahlige Werte bis 11 Stellen bzw. maximal 2147483647/4294967295
  // Tinyint: Ganzzahlige Werte bis 4 Stellen (in der Regel für 1 und 0 verwendet)
  // Bigint: wie Int, nur mit 20 Stellen bzw. maximal 9223372036854775807/18446744073709551615

  // Decimal: Fließkommazahlen

  // Date: Datum
  // Time: Zeit
  // Datetime: Datum und Zeit
  // Timestamp: Datum und Zeit, welche sich automatisch aktualisiert

  // Enum: Liste mit vordefinierten Werten (male, female)