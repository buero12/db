<?

  /**
   * Unter dem Begriff Normalisierung versteht man die logische Aufteilung von einer Tabelle auf mehrer Tabellen
   * Das Ziel ist es dabei die Tabelle möglichst redundant zu bekommen, also möglichst wenig Daten zu speichern, ohne das Informationen verloren gehen
   * Genauer gesagt, doppelte Daten sollte vermieden werden
   * Man verbindet mehrere Tabellen über sogenannte Fremdschlüssel miteinander (relationale Datenbanken)
   */

?>

<table>
  <thead>
    <tr>
      <th>ID</th>
      <th>Interpret</th>
      <th>Titel</th>
      <th>Jahr</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>Helene Fischer</td>
      <td>Von hier bis unendlich</td>
      <td>2006</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Helene Fischer</td>
      <td>So nah wie du</td>
      <td>2007</td>
    </tr>
    <tr>
      <td>3</td>
      <td>Iron Maiden</td>
      <td>The Final Frontier</td>
      <td>2014</td>
    </tr>
    <tr>
      <td>4</td>
      <td>Iron Maiden</td>
      <td>The Book of Souls</td>
      <td>2015</td>
    </tr>
  </tbody>
</table>

<hr />

<table>
  <thead>
    <tr>
      <th>ID</th>
      <th>Interpret ID</th>
      <th>Titel</th>
      <th>Jahr</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>1</td>
      <td>Von hier bis unendlich</td>
      <td>2006</td>
    </tr>
    <tr>
      <td>2</td>
      <td>1</td>
      <td>So nah wie du</td>
      <td>2007</td>
    </tr>
    <tr>
      <td>3</td>
      <td>2</td>
      <td>The Final Frontier</td>
      <td>2014</td>
    </tr>
    <tr>
      <td>4</td>
      <td>2</td>
      <td>The Book of Souls</td>
      <td>2015</td>
    </tr>
  </tbody>
</table>

<table>
  <thead>
    <tr>
      <th>ID</th>
      <th>Interpret</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>Helene Fischer</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Iron Maiden</td>
    </tr>
  </tbody>
</table>