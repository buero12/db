<?

  // Verbindung
  $pdo = include '../connection.php';

  /**
   * Joins dienen dazu, mehrere Tabellen miteinander zu verbinden
   */
  
  $stmt = $pdo->prepare('SELECT * FROM cd INNER JOIN artist ON artist_id = artist.id');
  $stmt->execute();
  
  $results = $stmt->fetchAll();
    
  print '<pre>';
  print_r($results);
  print '</pre>';
  
  /**
   * Man unterscheidet verschiedenen Arten von Joins
   * 
   * - inner join
   * - outer join
   * - left (inner|outer) join
   * - right (inner|outer) join
   */